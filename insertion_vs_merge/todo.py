#!/usr/bin/python3
import timeit
import insertion
import read_test
import merge_sort
from bokeh.plotting import *
import csv

"""
 import subprocess
 print "start"
 subprocess.call("./sleep.sh",shell=True)
 print "end"
"""

saltos=5
base=0
limite=500
rango_mayor=int(limite/saltos)+1
keys_time={}
inserts=[]
times=[]
for i in range(1,rango_mayor):
	base=base+saltos
	archivo="test_%d"%base+".test"
	keys=read_test.leer(archivo)
	start_time = timeit.default_timer()
	insertion.insertion(keys)
	elapsed = timeit.default_timer() - start_time
	time='{0:.10f}'.format(elapsed)
	inserts.append(base)
	times.append(float(time))

saltos=5
base=0
limite=500
rango_mayor=int(limite/saltos)+1
keys_time={}
inserts_merge=[]
times_merge=[]
for i in range(1,rango_mayor):
	base=base+saltos
	archivo="test_%d"%base+".test"
	keys=read_test.leer(archivo)
	start_time = timeit.default_timer()
	merge_sort.merge_sort(keys)
	elapsed = timeit.default_timer() - start_time
	time='{0:.10f}'.format(elapsed)
	inserts_merge.append(base)
	times_merge.append(float(time))



p=figure(plot_width=900, plot_height=600,title="INSERTION vs MERGE",x_axis_label='KEYS',y_axis_label='TIME')
p.title.text="INSERTION VS MERGE"
p.title.text_color="black"
p.title.text_font_size="25px"
p.title.align = "center"
p.line(inserts,times,legend="Insert sort",line_width=3, line_color="green")
p.circle(inserts,times,fill_color="white",size=3)
p.line(inserts_merge,times_merge,legend="Merge sort",line_width=3, line_color="red")
p.circle(inserts_merge,times_merge,fill_color="white",size=3)
output_file("deber.html")
show(p)


diccionario={}
with open('deber.csv', 'w') as csvfile:
    fieldnames = ['keys', 'insert','merge']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for i in range(0,len(inserts_merge)):
        diccionario['keys']=inserts[i]
        diccionario['insert']='{0:.10f}'.format(times[i])
        diccionario['merge']='{0:.10f}'.format(times_merge[i])
        writer.writerow(diccionario)
        diccionario.clear()