import read_test
import timeit
import csv
import insertion
import statistics
import merge_sort
import quick_sort
import gnome_sort
import heap_sort
import counting_sort
import shell_sort
from bokeh.plotting import *

def insertion_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    tiempos_array = []
    tiempo_insertion = []
    promedio = 0.0
    archivo = open('insertion.csv', 'wb')
    w = csv.writer(archivo)
    tiempos = []
    with open('insertion.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                insertion.sort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def quick_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    tiempos_array = []
    tiempo_insertion = []
    promedio = 0.0
    #archivo = open('merge.csv', 'wb')
    #w = csv.writer(archivo)
    tiempos = []
    with open('quick.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                quick_sort.quick_sort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def merge_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    promedio = 0.0
    tiempos = []
    with open('merge.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                merge_sort.merge_sort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def gnome_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    promedio = 0.0
    tiempos = []
    with open('gnome.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                gnome_sort.gnome_sort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def heap_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    promedio = 0.0
    tiempos = []
    with open('heap.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                heap_sort.heap_sort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def counting_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    promedio = 0.0
    tiempos = []
    with open('counting.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                counting_sort.countSort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def shell_generate():
    salto = 50
    contador = 50
    limite = 5000
    contador_diez=1
    promedio = 0.0
    tiempos = []
    with open('shell.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Files', 't1','t2','t3','t4','t5','t6','t7','t8','t9','t10','promedio'])
        while contador <= limite:
            tiempos.append(contador)
            while contador_diez <=10:
                arreglo = read_test.leer("test_"+str(contador)+"_"+str(contador_diez)+".test")
                start = timeit.default_timer()
                shell_sort.sort(arreglo)
                elapsed = timeit.default_timer() - start
                #elapsed_str='{0:.10f}'.format(elapsed)
                tiempos.append(elapsed)
                promedio = promedio + elapsed
                contador_diez = contador_diez + 1
            prom = statistics.median(tiempos[1:])
            tiempos.append(statistics.median(tiempos[1:]))
            spamwriter.writerow(tiempos)
            tiempos = []
            contador_diez = 1
            print(contador)
            contador = contador + salto

def insertion_read_file():
    array = []
    with open('insertion.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def quick_read_file():
    array = []
    with open('quick.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def merge_read_file():
    array = []
    with open('merge.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def gnome_read_file():
    array = []
    with open('gnome.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def heap_read_file():
    array = []
    with open('heap.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def counting_read_file():
    array = []
    with open('counting.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def shell_read_file():
    array = []
    with open('shell.csv','r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = list(reader)
    for row in rows:
       array.append(row['promedio'])
    return array

def print_grafico(insertion,quick,merge,gnome,heap,counting,shell):
    p=figure(plot_width=900, plot_height=600,title="INSERTION",x_axis_label='KEYS',y_axis_label='TIME')
    p.title.text="INSERTION"
    p.title.text_color="black"
    p.title.text_font_size="25px"
    p.title.align = "center"
    p.line(list(range(50,5050,50)),insertion,legend="Insert sort",line_width=3, line_color="green")
    p.circle(list(range(50,5050,50)),insertion,fill_color="white",size=3)
    p.line(list(range(50,5050,50)),quick,legend="quick sort",line_width=3, line_color="red")
    p.circle(list(range(50,5050,50)),quick,fill_color="white",size=3)
    p.line(list(range(50,5050,50)),merge,legend="merge sort",line_width=3, line_color="blue")
    p.circle(list(range(50,5050,50)),merge,fill_color="white",size=3)
    p.line(list(range(50,5050,50)),gnome,legend="gnome sort",line_width=3, line_color="yellow")
    p.circle(list(range(50,5050,50)),gnome,fill_color="white",size=3)
    p.line(list(range(50,5050,50)),heap,legend="heap sort",line_width=3, line_color="pink")
    p.circle(list(range(50,5050,50)),heap,fill_color="white",size=3)
    p.line(list(range(50,5050,50)),counting,legend="counting sort",line_width=3, line_color="orange")
    p.circle(list(range(50,5050,50)),counting,fill_color="white",size=3)
    p.line(list(range(50,5050,50)),shell,legend="shell sort",line_width=3, line_color="brown")
    p.circle(list(range(50,5050,50)),shell,fill_color="white",size=3)
    output_file("sorts.html")
    show(p)


if __name__ == '__main__':
    print_grafico(insertion_read_file(),quick_read_file(),merge_read_file(),gnome_read_file(),heap_read_file(),counting_read_file(),shell_read_file())
    #print (insertion_read_file())
    #insertion_generate()
    #quick_generate()
    #merge_generate()
    #gnome_generate()
    #heap_generate()
    #counting_generate()
    #shell_generate()
