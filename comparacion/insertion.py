def sort(A):
	j = 1
	for k in range(1,len(A)):
		key = A[j]
		i = j-1
		while i>=0 and A[i]>key:
			A[i+1]=A[i]
			i=i-1
		A[i+1]=key
		j=j+1
	return A

def sort_dec(A):
	j = 1
	for k in range(1,len(A)):
		key = A[j]
		i = j-1
		while i>=0 and A[i]<key:
			A[i+1]=A[i]
			i=i-1
		A[i+1]=key
		j=j+1
	return A
