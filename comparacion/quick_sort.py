def  quick_sort(array):
    if (len(array)) < 2:
        return array
    else:
        pivote = array[0]
        left = []
        right = []
        for j in array[1:]:
            if j < pivote:
                left.append(j)
            else:
                right.append(j)
        return quick_sort(left) + [pivote] + quick_sort(right)
