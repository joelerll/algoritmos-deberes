import numpy as np
minimo = 10
puntaje = [2,3,4,5,6,7]
esfuerzo = [2,3,4,5,6,8]
deberes = 6

a = np.zeros((deberes+1,minimo+1 ))

for i in range(1,a.shape[0]):
    for j in range(2,a.shape[1]):
        if(puntaje[i-2]<=j):
            if ((puntaje[i-1]+ a[i-1][j-puntaje[i-1]]) > a[i-1][j]):
                a[i][j] = esfuerzo[i-1] + a[i-1][j-puntaje[i-1]]
            else:
                a[i][j] = a[i-1][j]
        else:
            a[i][j] = a[i-1][j]

print(a)
