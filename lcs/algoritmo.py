import numpy

def lcs(string1, string2):
    tcadena1 = len(string1)
    tcadena2 = len(string2)
    m = [[0] * (1 + tcadena2) for i in range(1 + tcadena1)]
    lt = 0
    lx = 0
    for i in range(1, 1 + tcadena1):
        for j in range(1, 1 + tcadena2):
            if string1[i - 1] == string2[j - 1]:
                m[i][j] = m[i - 1][j - 1] + 1
                if m[i][j] > lt:
                    lt = m[i][j]
                    lx = i
            else:
                m[i][j] = 0
    return string1[lx - lt: lx]
